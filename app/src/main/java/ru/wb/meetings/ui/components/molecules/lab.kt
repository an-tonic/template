package ru.wb.meetings.ui.components.molecules

import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.tooling.preview.Preview
import ru.wb.meetings.R

@Preview
@Composable
fun TestFunction(modifier: Modifier = Modifier) {
    ExampleUsage(sizeOfCommunity = 2, formattedNumber = "20000")
}

@Composable
fun ExampleUsage(sizeOfCommunity: Int, formattedNumber: String) {
    val context = LocalContext.current

    // Get the plural string based on the quantity
    val communityString = context.resources.getQuantityString(
        R.plurals.community_people_count,
        sizeOfCommunity,
        formattedNumber
    )

    // Example usage in a Compose Text component
    Text(
        text = AnnotatedString(
            text = communityString,

        )
    )
}
