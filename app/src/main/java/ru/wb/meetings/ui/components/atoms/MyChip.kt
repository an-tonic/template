package ru.wb.meetings.ui.components.atoms

import androidx.compose.foundation.layout.height
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.FilterChip
import androidx.compose.material3.FilterChipDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import ru.wb.meetings.ui.theme.MyChipBodyColor
import ru.wb.meetings.ui.theme.MyChipTextColor
import ru.wb.meetings.ui.theme.TextStyle

@Preview(showBackground = true)
@Composable
fun MyChip(
    text: String = "Text",
    onClick: () -> Unit = { println("Unimplemented onClick") },
) {

    FilterChip(
        border = null,
        shape = RoundedCornerShape(50.dp),
        modifier = Modifier.height(20.dp),
        label = {
            Text(
                text,
                fontFamily = TextStyle.Metadata1.fontFamily,
                fontSize = TextStyle.Metadata1.fontSize,
            )
        },
        selected = false,
        onClick = onClick,

        colors = FilterChipDefaults.filterChipColors(
            containerColor = MyChipBodyColor, labelColor = MyChipTextColor
        )
    )
}