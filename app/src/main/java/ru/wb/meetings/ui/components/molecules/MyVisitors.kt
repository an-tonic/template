package ru.wb.meetings.ui.components.molecules

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.compose.ui.zIndex
import ru.wb.meetings.ui.components.atoms.MyEntityBorderedAvatar
import ru.wb.meetings.ui.theme.TextStyle


@Composable
fun MyVisitors(modifier: Modifier = Modifier, avatars: List<Int>) {

    Row(
        modifier = Modifier
            .width(216.dp)
            .padding(vertical = 4.dp),
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.SpaceBetween
    ) {
        Box(
        ) {
            avatars.forEachIndexed { index, avatarResourceId ->
                MyEntityBorderedAvatar(
                    modifier = Modifier
                        .offset(x = (index * 32).dp)
                        .zIndex(-index.toFloat()), // Ensure the first avatar is on top
                    avatarResourceId = avatarResourceId
                )
            }
        }
        Text(
            text = "+" + avatars.count(),
            fontFamily = TextStyle.BodyText1.fontFamily,
            fontSize = TextStyle.BodyText1.fontSize
        )
    }

}