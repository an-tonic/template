package ru.wb.meetings.ui.components

import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.interaction.PressInteraction
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import kotlinx.coroutines.launch
import ru.wb.meetings.ui.components.atoms.MyChip
import ru.wb.meetings.ui.components.atoms.MyGhostButton
import ru.wb.meetings.ui.components.atoms.MyPrimaryButton
import ru.wb.meetings.ui.components.atoms.MySearchBar
import ru.wb.meetings.ui.components.atoms.MySecondaryButton
import ru.wb.meetings.ui.components.atoms.MyTypography
import ru.wb.meetings.ui.components.molecules.MyAvatar
import ru.wb.meetings.ui.theme.TextStyle

@Preview(showBackground = true)
@Composable
fun MyFirstLesson() {
    LazyColumn(
        verticalArrangement = Arrangement.spacedBy(16.dp),
        contentPadding = PaddingValues(5.dp)
    ) {
        item {
            MyButtons()
        }
        item {
            MyTypographies()
        }
        item {
            MyAvatar()
        }
        item {
            MySearchBar()
        }
        item {
            MyChips()
        }
    }
}

@Composable
fun MyButtons() {
    Column {
        ButtonRow()
        ButtonRow(emulatePress = true)
        ButtonRow(isEnabled = false)
    }
}

@Composable
fun ButtonRow(
    isEnabled: Boolean = true,
    emulatePress: Boolean = false
) {
    val interactionSourceMock = remember { MutableInteractionSource() }

    if (emulatePress) {
        val coroutine = rememberCoroutineScope()

        LaunchedEffect(Unit) {
            coroutine.launch {
                val press = PressInteraction.Press(Offset.Zero)
                interactionSourceMock.emit(press)
            }
        }
    }

    Row(
        modifier = Modifier
            .fillMaxWidth(),
        horizontalArrangement = Arrangement.SpaceBetween
    ) {
        MyPrimaryButton(
            isEnabled = isEnabled,
            interactionSource = interactionSourceMock
        )
        MySecondaryButton(
            isEnabled = isEnabled,
            interactionSource = interactionSourceMock
        )
        MyGhostButton(
            isEnabled = isEnabled,
            interactionSource = interactionSourceMock
        )


    }
}




@Composable
fun MyTypographies() {
    Column(
        modifier = Modifier
            .padding(16.dp), verticalArrangement = Arrangement.spacedBy(16.dp)
    ) {
        val text = "The quick brown fox jumps over a lazy dog"
        MyTypography(text = text, style = TextStyle.Heading1)
        MyTypography(text = text, style = TextStyle.Heading2)
        MyTypography(text = text, style = TextStyle.Subheading1)
        MyTypography(text = text, style = TextStyle.Subheading2)
        MyTypography(text = text, style = TextStyle.BodyText1)
        MyTypography(text = text, style = TextStyle.BodyText2)
        MyTypography(text = text, style = TextStyle.Metadata1)
        MyTypography(text = text, style = TextStyle.Metadata2)
        MyTypography(text = text, style = TextStyle.Metadata3)
    }


}


@Composable
fun MyChips() {

    val myChipsList = listOf(
        "Python",
        "Junior",
        "Moscow"
    )

    Row {
        myChipsList.forEach {
            MyChip(text = it)
        }
    }
}




