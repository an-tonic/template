package ru.wb.meetings.ui.components.molecules

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import ru.wb.meetings.R
import ru.wb.meetings.ui.components.atoms.MyEntityAvatar
import ru.wb.meetings.ui.theme.MetadataTextGray
import ru.wb.meetings.ui.theme.TextStyle
import java.text.DecimalFormat

@Preview()
@Composable
fun MyCommunityCard(
    modifier: Modifier = Modifier,
    communityAvatarResourceId: Int = R.drawable.community_avatar,
    communityTitle: String = "Designa",
    communitySize: Int = 0
) {

    val decimalFormat = DecimalFormat("#,###")
    val formattedString = decimalFormat.format(communitySize).replace(",", " ")
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(horizontal = 8.dp)
            .background(Color.White)
    ) {
        MyEntityAvatar(avatarResourceId = communityAvatarResourceId)
        Spacer(modifier = Modifier.width(10.dp))
        Column {
            Text(
                text = communityTitle,
                fontFamily = TextStyle.BodyText1.fontFamily,
                fontSize = TextStyle.BodyText1.fontSize,
                modifier = Modifier.padding(bottom = 2.dp)
            )
            Text(
                text = LocalContext.current.resources.getQuantityString  (
                    R.plurals.community_people_count,
                    communitySize,
                    formattedString
                ),

                fontFamily = TextStyle.Metadata1.fontFamily,
                fontSize = TextStyle.Metadata1.fontSize,
                color = MetadataTextGray,
            )
        }
    }
}