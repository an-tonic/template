package ru.wb.meetings.ui.components.atoms

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.widthIn
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import ru.wb.meetings.ui.theme.TextStyle

@Composable
fun MyTypography(text: String, style: TextStyle) {
    LazyRow {
        item {
            Column(
                modifier = Modifier
                    .padding(end = 16.dp)
                    .widthIn(200.dp)
            ) {
                Text(
                    text = style.textStyleTitle,
                    fontSize = TextStyle.Subheading1.fontSize,
                    fontFamily = TextStyle.Subheading1.fontFamily
                )
                Text(
                    text = style.textStyleSubtitle,
                    fontSize = TextStyle.Metadata1.fontSize,
                    color = Color(0xFFB4BDC5),
                    fontFamily = TextStyle.Metadata1.fontFamily
                )
            }
        }
        item {
            Text(
                text = text,
                fontSize = style.fontSize,
                fontFamily = style.fontFamily
            )
        }
    }

}
