package ru.wb.meetings.ui.theme

import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.sp
import ru.wb.meetings.R


open class TextStyle(
    val textStyleTitle: String,
    val textStyleSubtitle: String,
    val fontFamily: FontFamily,
    val fontSize: TextUnit,
) {



    object Heading1 : TextStyle(
        "Heading 1",
        "SF Pro Display/32/Bold",
        FontFamily(Font(R.font.sf_pro_display_heavy, FontWeight.Bold)),
        32.sp
    )

    object Heading2 : TextStyle(
        "Heading 2",
        "SF Pro Display/24/Bold",
        FontFamily(Font(R.font.sf_pro_display_heavy, FontWeight.SemiBold)),
        24.sp
    )

    object Subheading1 : TextStyle(
        "Subheading 1",
        "SF Pro Display/18/SemiBold",
        FontFamily(Font(R.font.sf_pro_display_semibold, FontWeight.Medium)),
        18.sp
    )

    object Subheading2 : TextStyle(
        "Subheading 2",
        "SF Pro Display/16/SemiBold",
        FontFamily(Font(R.font.sf_pro_display_semibold, FontWeight.Medium)),
        16.sp
    )

    object BodyText1 : TextStyle(
        "Body Text 1",
        "SF Pro Display/14/SemiBold",
        FontFamily(Font(R.font.sf_pro_display_semibold, FontWeight.Medium)),
        14.sp
    )

    object BodyText2 : TextStyle(
        "Body Text 2",
        "SF Pro Display/14/Regular",
        FontFamily(Font(R.font.sf_pro_display_regular, FontWeight.Normal)),
        14.sp
    )
    object Metadata1 : TextStyle(
        "Metadata 1",
        "SF Pro Display/12/Regular",
        FontFamily(Font(R.font.sf_pro_display_regular, FontWeight.Normal)),
        12.sp
    )

    object Metadata2 : TextStyle(
        "Metadata 2",
        "SF Pro Display/10/Regular",
        FontFamily(Font(R.font.sf_pro_display_regular, FontWeight.Normal)),
        10.sp
    )

    object Metadata3 : TextStyle(
        "Metadata 3",
        "SF Pro Display/10/SemiBold",
        FontFamily(Font(R.font.sf_pro_display_semibold, FontWeight.Normal)),
        10.sp
    )

}
