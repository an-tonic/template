package ru.wb.meetings.ui.theme

import androidx.compose.ui.graphics.Color

val Purple80 = Color(0xFFD0BCFF)
val PurpleGrey80 = Color(0xFFCCC2DC)
val Pink80 = Color(0xFFEFB8C8)

val Purple40 = Color(0xFF6650a4)
val PurpleGrey40 = Color(0xFF625b71)
val Pink40 = Color(0xFF7D5260)


val MyNormalButtonColor = Color(0xFFA639FE)
val MyPressedButtonColor = Color(0xFF7000CF)
val MyDisabledButtonColor = Color(0xFFD29BFE)
val LightGray = Color(0xFFF6F6FB)
val MetadataTextGray = Color(0xFFADB5BD)
val AvatarBorderColor = Color(0xFFD2D5F9)
val MyChipBodyColor = Color(0xFFF5ECFF)
val MyChipTextColor = Color(0xFF660EC8)