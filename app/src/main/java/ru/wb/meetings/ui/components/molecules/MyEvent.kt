package ru.wb.meetings.ui.components.molecules

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import ru.wb.meetings.R
import ru.wb.meetings.ui.components.atoms.MyChip
import ru.wb.meetings.ui.components.atoms.MyEntityAvatar
import ru.wb.meetings.ui.theme.MetadataTextGray
import ru.wb.meetings.ui.theme.TextStyle

@Preview(showBackground = true, showSystemUi = true)
@Composable
fun MyEvent(
    modifier: Modifier = Modifier,
    avatarResourceId: Int = R.drawable.meeting_avatar,
    meetingTitle: String = "Developer meeting",
    date: String = "13.09.2024",
    place: String = "Москва",
    status: String = "",
    listOfCategories: List<String> = listOf("Python", "Junior", "Moscow")
) {

    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(vertical = 4.dp),
        horizontalArrangement = Arrangement.Start
    ) {
        MyEntityAvatar(avatarResourceId = avatarResourceId)
        Spacer(modifier = Modifier.width(10.dp))
        Column {

            Text(
                modifier = Modifier.height(24.dp),
                text = meetingTitle,
                fontFamily = TextStyle.BodyText1.fontFamily,
                fontSize = TextStyle.BodyText1.fontSize,
                color = Color(0xFF29183B)
            )
            Text(
                modifier = Modifier.height(20.dp),
                text = "$date — $place",
                fontFamily = TextStyle.Metadata1.fontFamily,
                fontSize = TextStyle.Metadata1.fontSize,
                color = MetadataTextGray,
            )

            LazyRow(
                modifier = Modifier.height(28.dp)
            ) {
                items(listOfCategories) { category ->
                    MyChip(text = category)
                }
            }
        }
    }

}