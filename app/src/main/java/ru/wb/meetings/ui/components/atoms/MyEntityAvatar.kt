package ru.wb.meetings.ui.components.atoms

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Image
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import ru.wb.meetings.R
import ru.wb.meetings.ui.theme.AvatarBorderColor

@Preview
@Composable
fun MyEntityAvatar(
    modifier: Modifier = Modifier,
    size: Dp = 48.dp,
    avatarResourceId: Int = R.drawable.meeting_avatar,
    contentDescription: String = "entity avatar image"
) {
    Box(
        modifier = Modifier
            .size(size)
            .clip(RoundedCornerShape(16.dp))
    ) {
        Image(
            painter = painterResource(id = avatarResourceId),
            contentDescription = contentDescription,
            contentScale = ContentScale.Crop
        )
    }
}

@Preview
@Composable
fun MyEntityBorderedAvatar(
    modifier: Modifier = Modifier,
    size: Dp = 48.dp,
    avatarResourceId: Int = R.drawable.meeting_avatar,
    contentDescription: String = "entity avatar image"
) {
    Box(
        modifier = modifier
            .border(BorderStroke(2.dp, AvatarBorderColor), shape = RoundedCornerShape(16.dp))
    ) {
        MyEntityAvatar(
            modifier = modifier,
            size = size,
            avatarResourceId = avatarResourceId,
            contentDescription = contentDescription
        )
    }
}