package ru.wb.meetings.ui.components

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.runtime.Composable
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import ru.wb.meetings.R
import ru.wb.meetings.ui.components.molecules.MyAvatar
import ru.wb.meetings.ui.components.molecules.MyCommunityCard
import ru.wb.meetings.ui.components.molecules.MyEvent
import ru.wb.meetings.ui.components.molecules.MyVisitors

@Preview(showBackground = true)
@Composable
fun MySecondLesson() {
    LazyColumn(
        verticalArrangement = Arrangement.spacedBy(16.dp),
        contentPadding = PaddingValues(5.dp)
    ) {
        item{
            MyEvent()
        }
        item {
            MyVisitors(avatars = listOf(R.drawable.person_avatar, R.drawable.person_avatar,R.drawable.person_avatar,R.drawable.person_avatar, R.drawable.person_avatar))
        }
        item {
            MyCommunityCard()
        }
        item {
            MyAvatar()
        }
        item {
           MyAvatar(isEdit = true)
        }

    }
}
