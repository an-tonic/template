package ru.wb.meetings.ui.components.atoms

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.interaction.collectIsPressedAsState
import androidx.compose.material.ripple.LocalRippleTheme
import androidx.compose.material.ripple.RippleAlpha
import androidx.compose.material.ripple.RippleTheme
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.OutlinedButton
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import ru.wb.meetings.ui.theme.MyDisabledButtonColor
import ru.wb.meetings.ui.theme.MyNormalButtonColor
import ru.wb.meetings.ui.theme.MyPressedButtonColor
import ru.wb.meetings.ui.theme.TextStyle


private object NoRippleTheme : RippleTheme {
    @Composable
    override fun defaultColor() = Color.Unspecified

    @Composable
    override fun rippleAlpha(): RippleAlpha = RippleAlpha(0.0f, 0.0f, 0.0f, 0.0f)
}

@Composable
fun NoRippleButton(content: @Composable () -> Unit) {
    CompositionLocalProvider(LocalRippleTheme provides NoRippleTheme) {
        content()
    }
}


@Composable
fun MyPrimaryButton(
    onClick: () -> Unit = { println("Unimplemented onClick") },
    text: String = "Button",
    isEnabled: Boolean = true,
    interactionSource: MutableInteractionSource = remember { MutableInteractionSource() }

) {
    val isPressed by interactionSource.collectIsPressedAsState()
    NoRippleButton {
        Button(
            onClick = onClick,
            enabled = isEnabled,
            interactionSource = interactionSource,

            colors = ButtonDefaults.buttonColors(
                contentColor = Color.White,
                containerColor = when {
                    isPressed -> MyPressedButtonColor
                    else -> MyNormalButtonColor
                },
                disabledContentColor = Color.White,
                disabledContainerColor = MyDisabledButtonColor

            )
        ) {
            Text(
                text = text,
                fontSize = TextStyle.Subheading2.fontSize,
                fontFamily = TextStyle.Subheading2.fontFamily
            )
        }
    }
}


@Composable
fun MySecondaryButton(
    onClick: () -> Unit = { println("Unimplemented onClick") },
    text: String = "Button",
    isEnabled: Boolean = true,
    interactionSource: MutableInteractionSource = remember { MutableInteractionSource() }
) {
    val isPressed by interactionSource.collectIsPressedAsState()
    NoRippleButton {
        OutlinedButton(
            onClick = onClick,
            enabled = isEnabled,
            interactionSource = interactionSource,
            border = when {
                isPressed -> BorderStroke(1.dp, MyPressedButtonColor)
                isEnabled -> BorderStroke(1.dp, MyNormalButtonColor)
                else -> BorderStroke(1.dp, MyDisabledButtonColor)
            },

            colors = ButtonDefaults.buttonColors(
                contentColor = when {
                    isPressed -> MyPressedButtonColor
                    else -> MyNormalButtonColor
                },
                containerColor = Color.White,
                disabledContentColor = MyDisabledButtonColor,
                disabledContainerColor = Color.White
            )
        ) {
            Text(
                text = text,
                fontSize = TextStyle.Subheading2.fontSize,
                fontFamily = TextStyle.Subheading2.fontFamily
            )
        }
    }

}

@Preview
@Composable
fun MyGhostButton(
    onClick: () -> Unit = { println("Unimplemented onClick") },
    text: String = "Button",
    isEnabled: Boolean = true,
    interactionSource: MutableInteractionSource = remember { MutableInteractionSource() }
) {
    val isPressed by interactionSource.collectIsPressedAsState()
    NoRippleButton {
        Button(
            onClick = onClick,
            enabled = isEnabled,
            interactionSource = interactionSource,
            colors = ButtonDefaults.buttonColors(
                contentColor = when {
                    isPressed -> MyPressedButtonColor
                    else -> MyNormalButtonColor
                },
                containerColor = Color.Transparent,
                disabledContentColor = MyDisabledButtonColor,
                disabledContainerColor = Color.Transparent
            )
        ) {
            Text(
                text = text,
                fontSize = TextStyle.Subheading2.fontSize,
                fontFamily = TextStyle.Subheading2.fontFamily
            )
        }
    }
}