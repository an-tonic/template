package ru.wb.meetings.ui.components.molecules

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material3.Icon
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import ru.wb.meetings.R
import ru.wb.meetings.ui.theme.LightGray


@Composable
fun MyAvatar(
    modifier: Modifier = Modifier,
    size: Dp = 100.dp,
    isEdit: Boolean = false,
    backgroundColor: Color = LightGray,
    painter: Painter = painterResource(id = R.drawable.avatar_icon),
) {

    val iconScale = size/2
    val iconPlusScale = size/5
    val iconPlusOffsetX = size/20

    Box(modifier = modifier) {
        Box(
            modifier = Modifier
                .size(size)
                .clip(CircleShape)
                .background(backgroundColor)
        ) {
            Icon(
                painter = painter,
                contentDescription = "avatar person",
                modifier = Modifier
                    .size(iconScale)
                    .align(Alignment.Center)
            )
        }
        if (isEdit) {
            Icon(
                painter = painterResource(id = R.drawable.avatar_plus),
                contentDescription = "plus sign",
                modifier = Modifier
                    .size(iconPlusScale)
                    .align(Alignment.BottomEnd)
                    .offset(-iconPlusOffsetX)

            )
        }
    }
}
